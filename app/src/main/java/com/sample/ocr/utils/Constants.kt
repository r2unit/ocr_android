package com.sample.ocr.utils

import okhttp3.MediaType
import java.util.concurrent.Executors

class Constants {
    companion object{
        val DEFAULT_TIME_OUT_SECONDS = 180L
        val MODEL_REQUEST_ID = 1
        val IMAGE_UPLOAD_REQUEST_ID = 101
        val PO_VERIFICATION_REQUEST_ID = 102
        val JSON = MediaType.get("application/json; charset=utf-8")
        val modelId = "65ba286a-f073-4ac0-be21-f74fac206777"
        //val modelId = "6b758c8d-85a6-4fe4-884f-f27c825a2d7a"
        val API_KEY = "u5XGC20xeCbuzPz7s2fPXOY-uRIvvpSo"
        //val API_KEY = "XGXt4v0JeN0DqPJ3d06pFizro1IGbV1JKhVYojIGice"
        val tagModelId="378732ae-77a4-42ee-b27a-b6303e103bc8"

        private val SINGLE_EXECUTOR = Executors.newSingleThreadExecutor()

        /**
         * Execute blocks on correct thread.
         */
        fun executeThread(f: () -> Unit) {
            SINGLE_EXECUTOR.execute(f)
        }

    }
}