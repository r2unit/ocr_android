package com.sample.ocr.ui.details

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sample.ocr.data.Invoice
import com.sample.ocr.callbacks.ItemCheckListener
import com.sample.ocr.R
import kotlinx.android.synthetic.main.layout_item_details.view.*


class DetailsAdapter(val itemCheckListener: ItemCheckListener) :

    RecyclerView.Adapter<DetailsAdapter.holder>() {

    var InvoiceList = ArrayList<Invoice>()
    lateinit var context: Context
    val DATE_REGEX_1 = "^[0-9]+(-[a-zA-Z_0-9]+)+\$".toRegex()
    val DATE_REGEX_2 = "^[0-9]+(/[a-zA-Z_0-9]+)+\$".toRegex()
    val DATE_REGEX_3 = "^[0-9]+(.[a-zA-Z_0-9]+)+\$".toRegex()

    val AMOUNT_REGEX_1 = "^\\d*\\.\\d+|\\d+\\.\\d*\$".toRegex()
    val AMOUNT_REGES_2 = "\\d+".toRegex()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.layout_item_details, parent, false)
        this.context = parent.context
        return holder(view)
    }

    override fun getItemCount(): Int {
        return InvoiceList.size
    }

    override fun onBindViewHolder(holder: holder, position: Int) {
        var invoice = InvoiceList[position]

       /* if(invoice.label == context.getString(R.string.invoice_number)
            ||(invoice.label == context.getString(R.string.invoice_date)&& matchesDateFormat(invoice.ocr_text))
            || invoice.label == context.getString(R.string.invoice_amount)*//*&& matchesAmountFormat(invoice.ocr_text)*//*) {
            holder.itemView.visibility = View.VISIBLE
            holder.itemView.layoutParams = RecyclerView.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            holder.itemView.tv_label.text = invoice.label
            holder.itemView.tv_value.text = invoice.ocr_text

            holder.itemView.checkbox_details.setOnCheckedChangeListener { buttonView, isChecked ->
                invoice.isChecked = isChecked
                itemCheckListener.onItemChecked(invoice)
            }
        }
        else{
            holder.itemView.visibility = View.GONE
            holder.itemView.layoutParams = RecyclerView.LayoutParams(0, 0)
        }*/
        holder.itemView.tv_label.text = invoice.label
        holder.itemView.tv_value.text = invoice.ocr_text

        holder.itemView.checkbox_details.isChecked = invoice.isChecked
    }

    class holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }


    fun updateList(newList: ArrayList<Invoice>) {
        InvoiceList = newList
        notifyDataSetChanged()
    }

    fun matchesDateFormat(dateString: String): Boolean{
        if(dateString.matches(DATE_REGEX_1) || dateString.matches(DATE_REGEX_2) || dateString.matches(DATE_REGEX_3)){
            return true
        }

        return false
    }

    fun matchesAmountFormat(amount: String): Boolean{
        if(amount.matches(AMOUNT_REGEX_1) || amount.matches(AMOUNT_REGES_2)){
            return true
        }

        return false
    }
}