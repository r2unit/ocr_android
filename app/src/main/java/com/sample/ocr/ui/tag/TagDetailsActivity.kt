package com.sample.ocr.ui.tag

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.sample.ocr.data.Invoice
import com.sample.ocr.callbacks.ItemCheckListener
import com.sample.ocr.R
import com.sample.ocr.ui.details.DataViewModel
import com.sample.ocr.ui.details.DetailsAdapter
import com.sample.ocr.ui.main.LandingActivity
import kotlinx.android.synthetic.main.activity_details.*

class TagDetailsActivity : AppCompatActivity(),
    ItemCheckListener {

    var detailsAdapter = DetailsAdapter(this)
    lateinit var invoiceList: ArrayList<Invoice>
    lateinit var viewModel: DataViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        supportActionBar!!.elevation = 0.0f

        viewModel = ViewModelProviders.of(this).get(DataViewModel::class.java)

        recycler_view_results.apply {
            layoutManager = LinearLayoutManager(this@TagDetailsActivity)
            adapter = detailsAdapter
        }

        invoiceList = intent.getSerializableExtra(getString(R.string.intent_list)) as ArrayList<Invoice>
        detailsAdapter.updateList(invoiceList)

        var po_number = intent.getStringExtra(getString(R.string.intent_po_number))
        btn_start_over.setOnClickListener {
            startActivity(Intent(this, LandingActivity::class.java))
            finishAffinity()
        }

        btn_verify.setOnClickListener {
            viewModel.verifyPoNumber(po_number)
        }
        btn_verify.visibility=View.GONE
    }

    override fun onItemChecked(inv: Invoice) {
        var index = invoiceList.indexOf(inv)
        var invoice = invoiceList[index]
        invoice.isChecked = inv.isChecked
        println(invoiceList)
    }


}
