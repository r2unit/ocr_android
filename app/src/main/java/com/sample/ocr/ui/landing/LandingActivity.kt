package com.sample.ocr.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sample.ocr.R
import com.sample.ocr.ui.tag.TagUploadActivity
import kotlinx.android.synthetic.main.activity_landing.*

class LandingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_landing)

        btn_uploadTag.setOnClickListener { startActivity( Intent(this, TagUploadActivity::class.java)) }
        btn_uploadInvoice.setOnClickListener { startActivity( Intent(this, MainActivity::class.java)) }
    }
}
