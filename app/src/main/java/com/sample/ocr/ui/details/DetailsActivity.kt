package com.sample.ocr.ui.details

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.sample.ocr.data.Invoice
import com.sample.ocr.callbacks.ItemCheckListener
import com.sample.ocr.R
import com.sample.ocr.ui.main.LandingActivity
import com.sample.ocr.ui.main.MainActivity
import com.sample.ocr.ui.main.MainViewModel
import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : AppCompatActivity(),
    ItemCheckListener {

    var detailsAdapter = DetailsAdapter(this)
    lateinit var invoiceList: ArrayList<Invoice>
    lateinit var viewModel: DataViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        supportActionBar!!.elevation = 0.0f

        viewModel = ViewModelProviders.of(this).get(DataViewModel::class.java)

        recycler_view_results.apply {
            layoutManager = LinearLayoutManager(this@DetailsActivity)
            adapter = detailsAdapter
        }

        invoiceList = intent.getSerializableExtra(getString(R.string.intent_list)) as ArrayList<Invoice>
        detailsAdapter.updateList(invoiceList)

        var po_number = intent.getStringExtra(getString(R.string.intent_po_number))
        btn_start_over.setOnClickListener {
            startActivity(Intent(this, LandingActivity::class.java))
            finishAffinity()
        }

        btn_verify.setOnClickListener {
            viewModel.verifyPoNumber(po_number)
        }


        viewModel.po_Date.observe(this, Observer {
            var index = invoiceList.indexOfFirst { it.label == getString(R.string.po_date)}
            if(index>=0) {
                var invoiceModel = invoiceList[index]
                invoiceModel.ocr_text = it
                if(!viewModel.isExpired) {
                    invoiceModel.isChecked = true
                }
            }
            else{
                var invoiceModel = Invoice(getString(R.string.po_date),it, true,0.0f,0.0f,0.0f,0.0f,0.0f)
                invoiceList.add(invoiceModel)
            }
            detailsAdapter.notifyDataSetChanged()
        })

        viewModel.po_Number.observe(this, Observer {
            var index = invoiceList.indexOfFirst { it.label == getString(R.string.po_number)}
            var invoiceModel = invoiceList[index]
            if(!viewModel.isExpired) {
                invoiceModel.isChecked = true
            }
            detailsAdapter.notifyDataSetChanged()
        })

        viewModel.errorMsg.observe(this, Observer {
            Toast.makeText(this,it,Toast.LENGTH_LONG).show()
        })

        viewModel.isLoading.observe(this, Observer {
            if(it){
                btn_start_over.visibility = View.GONE
                btn_verify.visibility = View.GONE
                progress_details.visibility = View.VISIBLE
            }
            else{
                btn_start_over.visibility = View.VISIBLE
                btn_verify.visibility = View.VISIBLE
                progress_details.visibility = View.GONE
            }
        })

    }

    override fun onItemChecked(inv: Invoice) {
        var index = invoiceList.indexOf(inv)
        var invoice = invoiceList[index]
        invoice.isChecked = inv.isChecked
        println(invoiceList)
    }


}
