package com.sample.ocr.ui.details

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.sample.ocr.network.NetworkRequestExecutor
import com.sample.ocr.utils.Constants
import com.task.ectosense.network.NetworkResponseListener
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class DataViewModel (application: Application) : AndroidViewModel(application),
    NetworkResponseListener {

    var isLoading = MutableLiveData<Boolean>()
    var po_url = "https://silverlinemsprod.iwizardsolutions.com:8003/api/v1/PurchaseOrderDetail?PoNumber="
    var po_Number = MutableLiveData<String>()
    var po_Date = MutableLiveData<String>()
    var errorMsg = MutableLiveData<String>()
    var isExpired: Boolean = false

    fun verifyPoNumber(poNumber: String){
        isLoading.postValue(true)

        var networkRequestExecutor = NetworkRequestExecutor()
        networkRequestExecutor.requestGET(po_url+poNumber, Constants.PO_VERIFICATION_REQUEST_ID,networkResponseListener = this)
    }

    override fun onSuccess(response: String, requestId: Int) {
        isLoading.postValue(false)
        var json = JSONObject(response)
        var respheader = json.optJSONObject("RespHeader")
        var statusCodee = respheader.optString("StatusCode")

        if(statusCodee == "200"){
            var bodyStr = json.optJSONObject("RespBody").optString("Payload")
            var body = JSONObject(bodyStr)

            isExpired = isPoExpired(body.optString("po_expiry"))

            po_Number.postValue(body.optString("po_number"))
            po_Date.postValue(body.optString("po_date"))
        }
        else{
            errorMsg.postValue("PO Number could not be matched")
        }
    }

    override fun onError(statusCode: Int, error: String, requestId: Int) {
        isLoading.postValue(false)
        errorMsg.postValue("PO Number could not be matched")
    }

    fun isPoExpired(expiryDate: String): Boolean {
        println("EXPIRY DATE: "+expiryDate)
        val  sdf =  SimpleDateFormat("dd-MM-yy");
        var expiryDate = sdf.parse(expiryDate);

        val currentDate = Date()
        var todaysDate = sdf.parse(sdf.format(currentDate))
        var i = expiryDate.compareTo(todaysDate);


        when {
            i>0 -> {
                return false
            }
            i<0 -> {
                //expired
                errorMsg.postValue("This PO has expired")
               return true
            }
        }
        return false
    }

}