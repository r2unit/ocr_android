package com.sample.ocr.ui.tag

import android.Manifest
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import androidx.lifecycle.Observer
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.androidisland.ezpermission.EzPermission
import com.sample.ocr.R
import com.sample.ocr.data.Invoice
import com.sample.ocr.ui.main.TagUploadViewModel
import gun0912.tedimagepicker.builder.TedImagePicker
import kotlinx.android.synthetic.main.activity_main.*
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.Serializable

class TagUploadActivity : AppCompatActivity() {

    lateinit var viewModel: TagUploadViewModel
    lateinit var imageName: String
    lateinit var base64: String
    var po_number=""
    val REQUEST_IMAGE_CAPTURE = 800
    val DATE_REGEX_1 = "^[0-9]+(-[a-zA-Z_0-9]+)+\$".toRegex()
    val DATE_REGEX_2 = "^[0-9]+(/[a-zA-Z_0-9]+)+\$".toRegex()
    val DATE_REGEX_3 = "^[0-9]+(.[a-zA-Z_0-9]+)+\$".toRegex()

    val AMOUNT_REGEX_1 = "^\\d*\\.\\d+|\\d+\\.\\d*\$".toRegex()
    val AMOUNT_REGES_2 = "\\d+".toRegex()
    val AMOUNT_REGEX_3 = ".*\\d+.*".toRegex()

    var invoices = ArrayList<Invoice>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tv_upload.text = getString(R.string.upload_tag)

        viewModel = ViewModelProviders.of(this).get(TagUploadViewModel::class.java)

        viewModel.isLoading.observe(this, Observer {
            if (it){
                btn_cancel.visibility = View.GONE
                btn_continue.visibility = View.GONE
                progress_bar.visibility = View.VISIBLE
                loading_message.visibility = View.VISIBLE
            }
            else{
                btn_cancel.visibility = View.VISIBLE
                btn_continue.visibility = View.VISIBLE
                progress_bar.visibility = View.GONE
                loading_message.visibility = View.GONE
            }
        })

        viewModel.errorMessage.observe(this, Observer {
            Toast.makeText(this@TagUploadActivity,it, Toast.LENGTH_LONG).show()
        })

        viewModel.loadingMessage.observe(this, Observer {
            loading_message.text = it
        })

        viewModel.allInvoiceList.observe(this, Observer {
            if(it.isNotEmpty()) {
                createObjects(it)
                var intent = Intent(this, TagDetailsActivity::class.java)
                intent.putExtra(getString(R.string.intent_list), invoices as Serializable?)
                intent.putExtra(getString(R.string.intent_po_number), po_number)
                startActivity(intent)
            }
        })

        logo_upload.setOnClickListener {
            EzPermission.with(this)
                .permissions(
                    Manifest.permission.CAMERA
                )
                .request { granted, denied, permanentlyDenied ->

                    TedImagePicker.with(this).savedDirectoryName("ocr")
                        .start { uri ->
                            var bitmap = handleSamplingAndRotationBitmap(this,uri)!!
                            constraint_main.visibility = View.GONE
                            constraint_upload.visibility = View.VISIBLE
                            image_invoice.setImageBitmap(bitmap)

                            val byteArrayOutputStream = ByteArrayOutputStream()
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
                            val byteArray: ByteArray = byteArrayOutputStream.toByteArray()
                            base64 = Base64.encodeToString(byteArray, Base64.DEFAULT);
                            imageName = uri.lastPathSegment!!
                        }
                }
        }

        btn_cancel.setOnClickListener {
            image_invoice.reset()
            constraint_upload.visibility = View.GONE
            constraint_main.visibility = View.VISIBLE
        }

        btn_continue.setOnClickListener {
            viewModel.uploadImage(imageName,base64)
        }
    }


    fun createObjects(arrayList: ArrayList<Invoice>){
        invoices.clear()
        var grouped = arrayList.groupBy { it.label }
        for (group in grouped){
            var label = group.key
            var value = ArrayList(group.value)
            var ocr_text = ""

            if(label == "Product_name"){
                for (number in value){
                    if(number.ocr_text.contains("\n")) {
                        ocr_text = number.ocr_text.replace("\n"," ")
                    }
                }
                var newInvoice = Invoice(label,ocr_text,false,0.0f,0.0f,0.0f,0.0f,0.0f)
                invoices.add(newInvoice)
            }
            else if(label == "P_o_code"){
                for (number in value){

                    ocr_text = number.ocr_text
                }
                var newInvoice = Invoice(label,ocr_text,false,0.0f,0.0f,0.0f,0.0f,0.0f)
                invoices.add(newInvoice)
            }
            else if(label == "address"){
                for (number in value){
                    if(number.ocr_text.contains("\n")) {
                        ocr_text = number.ocr_text.replace("\n"," ")
                    }
                }
                var newInvoice = Invoice(label,ocr_text,false,0.0f,0.0f,0.0f,0.0f,0.0f)
                invoices.add(newInvoice)
            }

            else if(label == "mrp"){
                for (number in value){
                    ocr_text = number.ocr_text
                }
                var newInvoice = Invoice(label,ocr_text,false,0.0f,0.0f,0.0f,0.0f,0.0f)
                invoices.add(newInvoice)
            }
        }

        println(invoices)

    }

    /*fun recognize(bitmap: Bitmap){
        val image = FirebaseVisionImage.fromBitmap(bitmap)
        val textRecognizer = FirebaseVision.getInstance().onDeviceTextRecognizer
        invoiceNo.visibility = View.GONE
        progress.visibility = View.VISIBLE
        textRecognizer.processImage(image)
            .addOnSuccessListener {
                val resultText = it.text
                for (block in it.textBlocks) {
                    val blockText = block.text

                    if(blockText.contains("Invoice No", ignoreCase = true) || blockText.contains("Invoice#", ignoreCase = true) || blockText.contains("Invoice Number", ignoreCase = true)) {
                        invoive_no = blockText
                        break
                        val blockConfidence = block.confidence
                        val blockCornerPoints = block.cornerPoints
                        val blockFrame = block.boundingBox
                        for (line in block.lines) {
                            val lineText = line.text
                            val lineConfidence = line.confidence
                            val lineCornerPoints = line.cornerPoints
                            val lineFrame = line.boundingBox
                            for (element in line.elements) {
                                val elementText = element.text
                                val elementConfidence = element.confidence
                                val elementCornerPoints = element.cornerPoints
                                val elementFrame = element.boundingBox
                            }
                        }


                    }
                    else{
                        invoive_no = ""
                    }
                }
                invoiceNo.text = invoive_no
                invoiceNo.visibility = View.VISIBLE
                progress.visibility = View.GONE
            }
            .addOnFailureListener {
                invoiceNo.text = "Invoice no could not be detected"
                invoiceNo.visibility = View.VISIBLE
                progress.visibility = View.GONE
            }


    }*/

    @Throws(IOException::class)
    fun handleSamplingAndRotationBitmap(context: Context, selectedImage: Uri): Bitmap? {
        val MAX_HEIGHT = 1024
        val MAX_WIDTH = 1024

        // First decode with inJustDecodeBounds=true to check dimensions
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        var imageStream = context.contentResolver.openInputStream(selectedImage)
        BitmapFactory.decodeStream(imageStream, null, options)
        imageStream!!.close()

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT)

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false
        imageStream = context.contentResolver.openInputStream(selectedImage)
        var img = BitmapFactory.decodeStream(imageStream, null, options)

        img = rotateImageIfRequired(context, img!!, selectedImage)
        return img
    }


    private fun calculateInSampleSize(
        options: BitmapFactory.Options,
        reqWidth: Int, reqHeight: Int
    ): Int {
        // Raw height and width of image
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            val heightRatio = Math.round(height.toFloat() / reqHeight.toFloat())
            val widthRatio = Math.round(width.toFloat() / reqWidth.toFloat())

            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            inSampleSize = if (heightRatio < widthRatio) heightRatio else widthRatio

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            val totalPixels = (width * height).toFloat()

            // Anything more than 2x the requested pixels we'll sample down further
            val totalReqPixelsCap = (reqWidth * reqHeight * 2).toFloat()

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++
            }
        }
        return inSampleSize
    }


    @Throws(IOException::class)
    private fun rotateImageIfRequired(context: Context, img: Bitmap, selectedImage: Uri): Bitmap {

        val input = context.contentResolver.openInputStream(selectedImage)
        val ei: ExifInterface
        if (Build.VERSION.SDK_INT > 23)
            ei = ExifInterface(input!!)
        else
            ei = ExifInterface(selectedImage.path!!)

        val orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)

        when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> return rotateImage(img, 90)
            ExifInterface.ORIENTATION_ROTATE_180 -> return rotateImage(img, 180)
            ExifInterface.ORIENTATION_ROTATE_270 -> return rotateImage(img, 270)
            else -> return img
        }
    }

    private fun rotateImage(img: Bitmap, degree: Int): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(degree.toFloat())
        val rotatedImg = Bitmap.createBitmap(img, 0, 0, img.width, img.height, matrix, true)
        img.recycle()
        return rotatedImg
    }
}
