package com.sample.ocr.ui.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.sample.ocr.data.Invoice
import com.sample.ocr.network.NetworkRequestExecutor
import com.sample.ocr.utils.Constants
import com.task.ectosense.network.NetworkResponseListener
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

class MainViewModel(application: Application) : AndroidViewModel(application),
    NetworkResponseListener {

    var isLoading = MutableLiveData<Boolean>()
    var errorMessage= MutableLiveData<String>()
    var loadingMessage= MutableLiveData<String>()
    var allInvoiceList = MutableLiveData<ArrayList<Invoice>>()
    //var image_url= "https://docs.devexpress.com/XtraReports/images/invoice-preview.png"
    //var image_url= "https://drive.google.com/file/d/1tFGnlIIy2M0nHMLSy_NPDCYPGN-7GzuE/"
    var nanonets_url = "https://app.nanonets.com/api/v2/OCR/Model/"+Constants.modelId+"/LabelUrls/"
    var blob_url = "https://silverlinems.iwizardsolutions.com:8003/api/v1/Upload/BlobFile"


    fun uploadImage(fileName: String, base64: String){
        isLoading.postValue(true)
        loadingMessage.postValue("Uploading Image")
        var payload = JSONObject()
        payload.put("CustomerId","5b8b3348-6ffe-4455-a369-25ab2a71b4a3")
        payload.put("FileName",fileName)
        payload.put("Base64",base64)
        payload.put("MessageId",UUID.randomUUID().toString())

        var networkRequestExecutor = NetworkRequestExecutor()
        networkRequestExecutor.requestPOST(blob_url, payload.toString(),
            Constants.IMAGE_UPLOAD_REQUEST_ID,networkResponseListener = this)
    }

    fun sendImageUrlForDetection(imageUrl: String){
        var networkRequestExecutor = NetworkRequestExecutor()
        networkRequestExecutor.requestDetect(nanonets_url, imageUrl,
            Constants.MODEL_REQUEST_ID,Constants.API_KEY, networkResponseListener = this)
    }

    override fun onSuccess(response: String, requestId: Int) {
        println("Response received for "+requestId)
        if(requestId == Constants.MODEL_REQUEST_ID) {
            processResponse(response)
        }
        else{
            loadingMessage.postValue("Upload complete, running detections")
            var json = JSONObject(convertStandardJSONString(response))
            if(json.optString("StatusCode") == "000") {
                var imageurl = json.optString("FileURI")
                println("FileUri: "+ imageurl )
                sendImageUrlForDetection(imageurl)
            }
        }
    }

    override fun onError(statusCode: Int, error: String, requestId: Int) {
        isLoading.postValue(false)
        errorMessage.postValue(error)
    }

    fun convertStandardJSONString(data_json: String): String? {
        var data_json = data_json
        data_json = data_json.replace("\\", "")
        data_json = data_json.replace("rn", "")
        data_json = data_json.substring(1, data_json.length - 1)
        return data_json
    }

    fun processResponse(response: String){
        isLoading.postValue(false)
        var json = JSONObject(response)
        var message = json.optString("message")
        if(!message.isNullOrEmpty() && message == "Success") {
            var result = json.optJSONArray("result")
            var obj = result[0] as JSONObject
            var predictionsArray = obj.optJSONArray("prediction")
            if(predictionsArray.length()>0){
                val type = object : TypeToken<List<Invoice?>?>() {}.type
                var predictionsList = Gson().fromJson<List<Invoice>>(predictionsArray.toString(), type)
                var invoiceList = ArrayList(predictionsList)
                allInvoiceList.postValue(invoiceList)
            }
        }
    }
}