package com.sample.ocr.callbacks

import com.sample.ocr.data.Invoice

interface ItemCheckListener {
    fun onItemChecked(invoice: Invoice)
}