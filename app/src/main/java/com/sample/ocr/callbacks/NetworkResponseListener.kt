package com.task.ectosense.network

import org.json.JSONException

interface NetworkResponseListener {

    @Throws(JSONException::class)
    fun onSuccess(response: String, requestId: Int)

    fun onError(statusCode: Int, error: String, requestId: Int)
}