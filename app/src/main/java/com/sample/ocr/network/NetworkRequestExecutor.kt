package com.sample.ocr.network

import android.os.Build
import android.os.Handler
import android.os.Looper
import com.sample.ocr.utils.Constants
import com.task.ectosense.network.NetworkResponseListener
import okhttp3.*
import java.io.IOException
import java.time.Duration
import java.util.concurrent.TimeUnit
import kotlin.concurrent.thread


internal class NetworkRequestExecutor() {

    /**
     * This method takes care of the GET requests called from the applications which are using this client. The 'encrypted', 'headers' and
     * 'timeout' fields are optional with their defined dafault values.
     */

    fun requestGET(url: String, reqId: Int, timeOut: Long = Constants.DEFAULT_TIME_OUT_SECONDS, networkResponseListener: NetworkResponseListener) {

        /**
         * Creating a request. The url and headers can be added to the request object directly
         */
        val request = Request.Builder()
            .url(url)
            .build()

        print("url:$url\nHeaders: \n RequestId: $reqId")
        /**
         * Creating a client. The timeout is fed to the client
         */
        val client = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            OkHttpClient().newBuilder().readTimeout(Duration.ofSeconds(timeOut)).build()
        } else {
            OkHttpClient().newBuilder().readTimeout(timeOut, TimeUnit.SECONDS).build()
        }


        /**
         * executing the request using the client
         */
        client.newCall(request).enqueue(object : Callback {

            var handler = Handler(Looper.getMainLooper())
            override fun onResponse(call: Call?, response: Response) {
                print("\n\nResponse for request: $reqId")
                    if (response.isSuccessful) {
                       thread {
                            val responseData = response.body()?.string()
                            networkResponseListener.onSuccess(responseData!!, reqId)
                        }
                    } else {
                        networkResponseListener.onError(response.code(), response.body().toString(), reqId)
                    }
            }

            override fun onFailure(call: Call?, e: IOException?) {
                handler.post {
                    e!!.printStackTrace()
                    val error = e.toString()
                    networkResponseListener.onError(0, error, reqId)
                }
            }
        })
    }


    /**
     * This method takes care of the POST requests called from the applications which are using this client. The 'encrypted', 'headers' and
     * 'timeout' fields are optional with their defined dafault values.
     */

    fun requestDetect(url: String, imageUrl: String, reqId: Int, apiKey: String, timeOut: Long = Constants.DEFAULT_TIME_OUT_SECONDS, networkResponseListener: NetworkResponseListener) {

        val body: RequestBody = FormBody.Builder().add("urls", imageUrl).build()


        /**
         * Creating a request. The url and headers can be added to the request object directly
         */
        val request = Request.Builder()
            .url(url)
            .post(body)
            .addHeader("Authorization", Credentials.basic(apiKey, ""))
            .build()

        print("url:$url\nBody: $imageUrl\n RequestId: $reqId")

        /**
         * Creating a client. The timeout is fed to the client
         */
        val client = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            OkHttpClient().newBuilder().readTimeout(Duration.ofSeconds(timeOut)).build()
        } else {
            OkHttpClient().newBuilder().readTimeout(timeOut, TimeUnit.SECONDS).build()
        }


        /**
         * executing the request using the client
         */
        client.newCall(request).enqueue(object : Callback {

            var handler = Handler(Looper.getMainLooper())

            override fun onResponse(call: Call?, response: Response) {
                print("\nResponse for request: $reqId")
                handler.post {
                    if (response.isSuccessful) {
                        val responseData = response.body()?.string()
                        networkResponseListener.onSuccess(responseData!!,reqId)
                    } else {
                        var error = response.body().toString()
                        networkResponseListener.onError(response.code(), response.message(), reqId)
                    }
                }
            }

            override fun onFailure(call: Call?, e: IOException?) {
                handler.post {
                    e!!.printStackTrace()
                    val error = e.toString()
                    networkResponseListener.onError(0, error, reqId)
                }
            }
        })
    }



    fun requestPOST(url: String, json: String, reqId: Int, timeOut: Long = Constants.DEFAULT_TIME_OUT_SECONDS, networkResponseListener: NetworkResponseListener) {

        val body = RequestBody.create(Constants.JSON, json)

        /**
         * Creating a request. The url and headers can be added to the request object directly
         */
        val request = Request.Builder()
            .url(url)
            .post(body)
            .build()

        print("url:$url\n RequestId: $reqId")

        /**
         * Creating a client. The timeout is fed to the client
         */
        val client = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            OkHttpClient().newBuilder().readTimeout(Duration.ofSeconds(timeOut)).build()
        } else {
            OkHttpClient().newBuilder().readTimeout(timeOut, TimeUnit.SECONDS).build()
        }


        /**
         * executing the request using the client
         */
        client.newCall(request).enqueue(object : Callback {

            var handler = Handler(Looper.getMainLooper())

            override fun onResponse(call: Call?, response: Response) {
                print("\nResponse for request: $reqId")
                handler.post {
                    if (response.isSuccessful) {
                        val responseData = response.body()?.string()
                        networkResponseListener.onSuccess(responseData!!,reqId)
                    } else {
                        var error = response.body().toString()
                        networkResponseListener.onError(response.code(), response.message(), reqId)
                    }
                }
            }

            override fun onFailure(call: Call?, e: IOException?) {
                handler.post {
                    e!!.printStackTrace()
                    val error = e.toString()
                    networkResponseListener.onError(0, error, reqId)
                }
            }
        })
    }

}