package com.sample.ocr.data

import java.io.Serializable

data class Invoice(
    val label: String,
    var ocr_text: String,
    var isChecked: Boolean,
    val score: Float,
    val xmax: Float,
    val xmin: Float,
    val ymax: Float,
    val ymin: Float
): Serializable